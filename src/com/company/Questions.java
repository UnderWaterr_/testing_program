package com.company;

import java.io.*;

public class Questions implements Serializable {
    String text;
    String[] answers;
    String[] correctAnswers;
    float points;

    public Questions(String text, String[] answers, String[] correctAnswers, float points) {
        this.text = text;
        this.answers = answers;
        this.correctAnswers = correctAnswers;
        this.points = points;
    }

    public String[] getAnswers() {
        return answers;
    }

    public String[] getCorrectAnswers() {
        return correctAnswers;
    }

    public void setPoints(float points) {
        this.points = points;
    }

    public float getPoints() {
        return points;
    }
}
