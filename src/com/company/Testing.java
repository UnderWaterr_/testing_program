package com.company;
import java.text.SimpleDateFormat;
import java.util.*;

public class Testing {

    public String[][] testing(ArrayList<Questions> questions, int numberOfQuestions) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Тест запущен. \n");
        String[][] answers = new String[numberOfQuestions][];
        String[][] answersUser = new String[numberOfQuestions][];
        for (int i = 0; i < numberOfQuestions; i++) {
            System.out.println((i + 1) + " вопрос: " + questions.get(i).text + "\nВарианты ответа:");
            for (int j = 0; j < questions.get(i).getAnswers().length; j++) {
                System.out.println(j + 1 + ". " + questions.get(i).getAnswers()[j]);
            }
            System.out.println("Выберите верные варианты ответа:");
            boolean flagAnswersUser = true;
            while (flagAnswersUser) {
                try {
                    String key = scanner.nextLine();
                    answers[i] = key.split(" ");
                    answersUser[i] = answers[i];
                    for (int j = 0; j < answers[i].length; j++) {
                        answersUser[i][j] = questions.get(i).getAnswers()[Integer.parseInt(answers[i][j].replaceAll("\\s+", "")) - 1];
                    }
                    if (answersUser[i].length > questions.get(i).correctAnswers.length ){
                        throw new IndexOutOfBoundsException();
                    }
                    System.out.println();
                    flagAnswersUser = false;
                } catch (NullPointerException e) {
                    System.out.println("Вопроса с таким номером не существует.");
                } catch (NumberFormatException e) {
                    System.out.println("Неверный формат.");
                } catch (ArrayIndexOutOfBoundsException a) {
                    System.out.println("Ответа с таким номером не существует.");
                }catch (IndexOutOfBoundsException ix){
                    System.out.println("Вы ввели слишком много правильных ответов.");
                }
            }
        }
        return answersUser;
    }

    public float checkAnswers(User user, int mark) {
        float coast = (float) mark / user.answer.length;
        float score = 0;
        String[][] rightAnswers = new String[user.questions.size()][];
        String[][] wrongAnswers = new String[user.questions.size()][];
        ArrayList<String> rightAnswer = new ArrayList<>();
        ArrayList<String> wrongAnswer = new ArrayList<>();
        for (Questions questions : user.questions) {
            questions.setPoints(coast / questions.getCorrectAnswers().length);
        }
        for (int i = 0; i < user.answer.length; i++) {
            for (int j = 0; j < user.questions.get(i).getCorrectAnswers().length; j++) {
                for (int k = 0; k < user.answer[i].length; k++) {
                    if (user.questions.get(i).getCorrectAnswers()[j].equals(user.answer[i][k])) {
                        score = score + user.questions.get(i).getPoints();
                        rightAnswer.add(user.answer[i][k]);
                        break;
                    } else if (k == ((user.answer[i].length) - 1)) {
                        wrongAnswer.add(user.answer[i][k]);
                    }
                }
            }
            String[] qq = new String[rightAnswer.size()];
            for (int j = 0; j < rightAnswer.size(); j++) {
                qq[j] = rightAnswer.get(j);
            }
            String[] qqq = new String[wrongAnswer.size()];
            for (int j = 0; j < wrongAnswer.size(); j++) {
                qqq[j] = wrongAnswer.get(j);
            }
            rightAnswers[i] = qq;
            wrongAnswers[i] = qqq;
            rightAnswer.clear();
            wrongAnswer.clear();
        }
        return displayResults(user, rightAnswers, wrongAnswers, score);
    }

    public float displayResults(User user, String[][] rightAnswers, String[][] wrongAnswers, float score) {
        for (int i = 0; i < user.questions.size(); i++) {
            System.out.println("\n" + (i + 1) + " вопрос: " + user.questions.get(i).text + "\nВарианты ответа");
            for (int j = 0; j < user.questions.get(i).getAnswers().length; j++) {
                int q = 0;
                for (int k = 0; k < rightAnswers[i].length; k++) {
                    if (user.questions.get(i).getAnswers()[j].equals(rightAnswers[i][k])) {
                        System.out.println("\u001b[32m" + (j + 1) + ". " + user.questions.get(i).getAnswers()[j] + "\u001b[0m");
                        q++;
                        break;
                    }
                }
                if (q == 0) {
                    for (int k = 0; k < wrongAnswers[i].length; k++) {
                        if (user.questions.get(i).getAnswers()[j].equals(wrongAnswers[i][k])) {
                            System.out.println("\u001b[31m" + (j + 1) + ". " + user.questions.get(i).getAnswers()[j] + "\u001b[0m");
                            q++;
                            break;
                        }
                    }
                }
                if (q == 0) {
                    System.out.println((j + 1) + ". " + user.questions.get(i).getAnswers()[j]);
                }
            }
        }
        return score;

    }

    public void testing(ArrayList<Questions> questions) {
        Scanner scanner = new Scanner(System.in);
        Testing testing = new Testing();
        ArrayList<Questions> shuffleQuestions = new ArrayList<>();
        System.out.println("Введите количество баллов которое можно получить за тест.");
        int mark = 0;
        boolean flagMark = true;
        while (flagMark) {
            try {
                mark = Integer.parseInt(scanner.next());
                flagMark = false;
            } catch (NumberFormatException e) {
                System.out.println("Неверный формат.");
            }
        }

        System.out.println("Доступно вопросов: " + questions.size());
        shuffleQuestions(questions);

        System.out.println("Введите желаемое количество вопросов в тесте.");

        Date start = new Date();
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        timeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        int numberOfQuestions = 0;
        boolean flagNumberOfQuestions = true;
        while (flagNumberOfQuestions)
            try {
                numberOfQuestions = Integer.parseInt(scanner.next());
                if (numberOfQuestions > questions.size()) {
                    throw new IndexOutOfBoundsException();
                }
                flagNumberOfQuestions = false;
            } catch (IndexOutOfBoundsException e) {
                System.out.println("Слишком большое количество вопросов.");
            } catch (NumberFormatException e) {
                System.out.println("Неверный формат.");
            }
        for (int i = 0; i < numberOfQuestions; i++) {
            shuffleQuestions.add(questions.get(i));
        }
        User user = new User(shuffleQuestions, testing.testing(shuffleQuestions, numberOfQuestions));

        System.out.println();
        System.out.println("\nВы получили за тест: " + testing.checkAnswers(user, mark) + "\n");
        Date finish = new Date();
        String timeString = timeFormat.format(new Date(finish.getTime() - start.getTime()));
        System.out.println("Вы закончили тест за: " + timeString);
    }



    public ArrayList<Questions> creatingQuestions(int numberOfQuestions) {
        Scanner scanner = new Scanner(System.in);
        String text;
        int numberOfAnswers = 0;

        ArrayList<Questions> questions = new ArrayList<>();
        for (int i = 0; i < numberOfQuestions; i++) {
            System.out.println("Введите текст " + (i + 1) + " вопроса.");
            text = scanner.nextLine();
            System.out.println("Введите количество вариантов ответа.");
            boolean flagNumberOfAnswers = true;

            while (flagNumberOfAnswers) {
                try {
                    numberOfAnswers = Integer.parseInt(scanner.nextLine());
                    if (numberOfAnswers <= 1) {
                        throw new NullPointerException();
                    }
                    flagNumberOfAnswers = false;
                } catch (NumberFormatException e) {
                    System.out.println("Неверный формат ввода данных.");
                } catch (NullPointerException e) {
                    System.out.println("Количество вариантов ответа >1.");
                }
            }

            String[] answers = new String[numberOfAnswers];

            System.out.println("Введите варианты ответа.");
            for (int j = 0; j < numberOfAnswers; j++) {
                System.out.print(j + 1 + ". ");
                answers[j] = scanner.nextLine();
            }

            System.out.println("Введите номер верных вариантов ответа через пробел.");

            boolean flagNumberOfCorrectAnswer = true;
            while (flagNumberOfCorrectAnswer) {
                try {
                    String key = scanner.nextLine();
                    String[] correctAnswers = key.split(" ");
                    Arrays.sort(correctAnswers);
                    if (correctAnswers.length >= numberOfAnswers) {
                        throw new NullPointerException();
                    }
                    String[] correctAnswersString = new String[correctAnswers.length];


                    for (int j = 0; j < correctAnswers.length; j++) {
                        correctAnswersString[j] = answers[Integer.parseInt(correctAnswers[j].replaceAll("\\s+", "")) - 1];
                    }
                    questions.add(new Questions(text, answers, correctAnswersString, 0));

                    flagNumberOfCorrectAnswer = false;
                } catch (NullPointerException e) {
                    System.out.println("Количество правильных ответов >= вопросов.");
                } catch (NumberFormatException e) {
                    System.out.println("Неверный формат.");
                }
            }

        }
        return questions;
    }





    public ArrayList<Questions> remoteQuestion(ArrayList<Questions> questions) {
        ArrayList<Questions> remoteQuestions = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        String[] remoteQuestion;
        int[] remoteQuestionInt = new int[0];
        System.out.println("Введите удаляемые вопросы через пробел");
        boolean flagRemoteQuestion = true;
        while (flagRemoteQuestion) {
            try {
                String remoteQuestionUser = scanner.nextLine();
                remoteQuestion = remoteQuestionUser.split(" ");

                remoteQuestionInt = new int[remoteQuestion.length];
                for (int i = 0; i < remoteQuestion.length; i++) {
                    remoteQuestionInt[i] = Integer.parseInt(remoteQuestion[i]) - 1;
                    if (remoteQuestionInt[i] > questions.size()) {
                        throw new NullPointerException();
                    }
                }

                flagRemoteQuestion = false;
            } catch (NullPointerException e) {
                System.out.println("Вопроса с таким номером не существует.");
            } catch (NumberFormatException e) {
                System.out.println("Неверный формат.");
            }
        }
        Arrays.sort(remoteQuestionInt);
        int count = 0;

        for (int i = 0; i < questions.size(); i++) {
            if (remoteQuestionInt[count] != i) {
                remoteQuestions.add(questions.get(i));
            } else if (count < remoteQuestionInt.length - 1) {
                count++;
            }
        }
        return remoteQuestions;
    }

    public void shuffleQuestions(ArrayList<Questions> questions) {
        Random rnd = new Random();
        for (int i = 0; i < questions.size(); i++) {
            int index = rnd.nextInt(i + 1);
            Questions a = questions.get(i);
            questions.set(i, questions.get(index));
            questions.set(index, a);
        }
    }

    public void showQuestions(ArrayList<Questions> questions) {
        for (int i = 0; i < questions.size(); i++) {
            System.out.println("\n" + (i + 1) + " вопрос: " + questions.get(i).text + "\nВарианты ответа:");
            for (int j = 0; j < questions.get(i).getAnswers().length; j++) {
                System.out.println(j + 1 + ". " + questions.get(i).getAnswers()[j]);
            }
        }
    }
}
