package com.company;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String filename = "questions.dat";
        String filenameWithBasicQuestions = "BasicQuestions.dat";
        Testing testing = new Testing();
        ArrayList<Questions> questions = readQuestions(filename, filenameWithBasicQuestions);
        boolean on = true;
        while (on) {
            System.out.println("\n1. Создать новые вопросы. \n2. Начать тестирование. \n3. Очистить базу вопросов." +
                    "\n4. Удалить вопросы.\n5. Вывести все вопросы.\n6. Сбросить вопросы до базовых.\n7. Завершить.");
            switch (scanner.next()) {
                case ("1"):
                    writeQuestions(questions, filename, filenameWithBasicQuestions, testing);
                    questions = readQuestions(filename, filenameWithBasicQuestions);
                    break;
                case ("2"):
                    questions = readQuestions(filename, filenameWithBasicQuestions);
                    testing.testing(questions);
                    break;
                case ("3"):
                    clearQuestions(filename);
                    break;
                case ("4"):
                    questions = readQuestions(filename, filenameWithBasicQuestions);
                    addQuestions(testing.remoteQuestion(questions), filename, filenameWithBasicQuestions, false);
                    questions = readQuestions(filename, filenameWithBasicQuestions);
                    break;
                case ("5"):
                    questions = readQuestions(filename, filenameWithBasicQuestions);
                    testing.showQuestions(questions);
                    break;
                case ("6"):
                    addQuestions(questions, filename, filenameWithBasicQuestions, true);
                    break;
                case ("7"):
                    on = false;
                    break;
            }
        }

    }


    public static void writeQuestions(ArrayList<Questions> questions, String filename, String filenameWithBasicQuestions ,Testing testing) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество вопросов, которое хотите создать.");
        boolean flag = true;
        int numberOfQuestions = 0;
        while (flag) {
            try {
                numberOfQuestions = Integer.parseInt(scanner.nextLine());
                flag = false;
            } catch (NumberFormatException i) {
                System.out.println("Неверный формат.");
            }
        }
        questions.addAll(testing.creatingQuestions(numberOfQuestions));

        addQuestions(questions, filename, filenameWithBasicQuestions, false);

    }

    public static void addQuestions(ArrayList<Questions> questions, String filename, String filenameWithBasicQuestions, boolean flag) {
        if (flag) {
            clearQuestions(filename);
            try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filename))) {
                oos.writeObject(readQuestions(null, filenameWithBasicQuestions));
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }else{
            try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filename))) {
                oos.writeObject(questions);
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    public static void clearQuestions(String filename) {
        ArrayList<Questions> questions = new ArrayList<>();
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filename))) {
            oos.writeObject(questions);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static ArrayList<Questions> readQuestions(String filenameWithQuestions, String filenameWithBasicQuestions) {
        ArrayList<Questions> questions = new ArrayList<>();
        if (filenameWithQuestions!=null){
            try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filenameWithQuestions))) {
                questions = ((ArrayList<Questions>) ois.readObject());
            } catch (Exception ex) {
                System.out.println();
            }
        }else{
            try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filenameWithBasicQuestions))) {
                questions = ((ArrayList<Questions>) ois.readObject());
            } catch (Exception ex) {
                System.out.println();
            }
        }

        return questions;
    }
}