package com.company;

import java.util.ArrayList;

public class User {
    ArrayList<Questions> questions;
    String[][] answer;

    public User(ArrayList<Questions> questions, String[][] answer) {
        this.questions = questions;
        this.answer = answer;
    }
}
